

resource "aws_security_group" "my_sg" {
    ingress{
        cidr_blocks = ["0.0.0.0/0"]
        protocol    = "tcp"
        from_port   = "22"
        to_port     = "22"
    }
    ingress{
        cidr_blocks = ["0.0.0.0/0"]
        protocol    = "tcp"
        from_port   = "3000"
        to_port     = "3000"
    }
    ingress {
    # chef default pport 
    cidr_blocks = ["0.0.0.0/0"]
    protocol    = "tcp"
    from_port   = 443
    to_port     = 443
    
  }

    egress{
        cidr_blocks = ["0.0.0.0/0"]
        protocol    = "-1"
        from_port   = "0"
        to_port     = "0"
    }
}
resource "aws_instance" "Grafana" {
    ami                         =  var.Grafana
    instance_type               = "t2.micro"
    associate_public_ip_address = true
    vpc_security_group_ids      = [aws_security_group.my_sg.id]
    key_name                    = var.awskeypair
   
     connection {
        type        = "ssh"
        user        = var.sshusername
        private_key = file(var.sshkeypath)
        host        = aws_instance.Grafana.public_ip
    }

     provisioner "remote-exec" {
    inline = [
	"sudo apt update -y",
    "wget https://dl.grafana.com/oss/release/grafana_6.5.2_amd64.deb",
"sudo apt-get install -y adduser libfontconfig",
"sudo apt --fix-broken install",
"sudo dpkg -i grafana_6.5.2_amd64.deb",
"sh -c 'echo 'deb https://packagecloud.io/grafana/stable/debian/ stretch main' >> /etc/apt/sources.list'",
"curl https://packagecloud.io/gpg.key | sudo apt-key add -",
"sudo apt-get update",
"sudo apt-get install grafana",
"sudo service grafana-server start",
"sudo update-rc.d grafana-server defaults",
"sudo systemctl enable grafana-server.service",
    ]
  }
   tags = {
    Name = "Grafana"
  }
}